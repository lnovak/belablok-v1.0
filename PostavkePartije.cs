﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace BelaBlok
{
    [Serializable]
    public class PostavkePartije
    {

        public string STim1 { get; set; }
        public string STim2 { get; set; }
        public int MaxBodovi { get; set; }

        public PostavkePartije(){}

        public PostavkePartije(string tim1, string tim2, int max) {
            STim1 = tim1;
            STim2 = tim2;
            MaxBodovi = max;
        }

        public void CitajPostavke()
        {
            try
            {
                string json;
                PostavkePartije p;
                json = File.ReadAllText("Postavke.json");
                p = JsonSerializer.Deserialize<PostavkePartije>(json);
                STim1 = p.STim1;
                STim2 = p.STim2;
                MaxBodovi = p.MaxBodovi;
            }
            catch (Exception e)
            {
                Console.WriteLine("Greška kod čitanja postavki: ");
                Console.WriteLine(e.Message);
            }
        }

        public void PisiPostavke(string tim1, string tim2, int max)
        {
            STim1 = tim1;
            STim2 = tim2;
            MaxBodovi = max;
            try
            {
                using (StreamWriter wr = new StreamWriter("Postavke.json"))
                {
                    string json = JsonSerializer.Serialize(this);
                    wr.Write(json);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Greška kod pisanja postavki: ");
                Console.WriteLine(e.Message);
            }
        }

    }
}

