﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BelaBlok
{
    public partial class Unos : Form
    {
        //public Engine engine;
        public int Karte1 { get; set; }
        public int Karte2 { get; set; }
        public int Zvanja1 { get; set; }
        public int Zvanja2 { get; set; }

        Engine engine = new Engine();
        public Unos()
        {
            InitializeComponent();
        }

        private void r1_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void bOdustani_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bZapisi_Click(object sender, EventArgs e)
        {
            if (!tIgra1.Text.All(char.IsDigit)|| !tIgra2.Text.All(char.IsDigit)|| !tZvanja1.Text.All(char.IsDigit)|| !tZvanja2.Text.All(char.IsDigit))
            {
                MessageBox.Show("Bodovi ne smiju sadržavati znakove");
            }
            else
            {
                if (tIgra1.Text == "" || tIgra1.Text == " ") { tIgra1.Text = "0"; }
                if (tIgra2.Text == "" || tIgra2.Text == " ") { tIgra2.Text = "0"; }
                if (tZvanja1.Text == "" || tZvanja1.Text == " ") { tZvanja1.Text = "0"; }
                if (tZvanja2.Text == "" || tZvanja2.Text == " ") { tZvanja2.Text = "0"; }
                Karte1 = Int32.Parse(tIgra1.Text);
                Zvanja1 = Int32.Parse(tZvanja1.Text);
                Karte2 = Int32.Parse(tIgra2.Text);
                Zvanja2 = Int32.Parse(tZvanja2.Text);
                this.Close();
            }
        }

        private void Unos_Load(object sender, EventArgs e)
        {
            tTim1.Text = engine.STim1;
            tTim2.Text = engine.STim2;
        }

        private void tIgra1_TextChanged(object sender, EventArgs e)
        {

        }

        private void tIgra1_Click(object sender, EventArgs e)
        {
            if(tIgra2.Text != "" && Int32.Parse(tIgra2.Text) < 162)
            {
                tIgra1.Text = (162 - Int32.Parse(tIgra2.Text)).ToString();
            }
        }

        private void tIgra2_Click(object sender, EventArgs e)
        {
            if (tIgra1.Text != "" && Int32.Parse(tIgra1.Text) < 162)
            {
                tIgra2.Text = (162 - Int32.Parse(tIgra1.Text)).ToString();
            }
        }
    }
}
