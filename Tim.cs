﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BelaBlok
{
    public class Tim
    {
        public string Ime { get; set; }
        public int Pobjede { get; set; }
        public int Karte { get; set; }
        public int Zvanja { get; set; }
        public int Runda { get; set; }
        public List<int> Bodovi { get; set; }

        public Tim(string ime)
        {
            Ime = ime;
            Pobjede = 0;
            Karte = 0;
            Zvanja = 0;
            Runda = 0;
            Bodovi = new List<int>();
        }

        public int Suma()
        {
            int suma = 0;
            foreach (int runda in Bodovi)
            {
                suma += runda;
            }
            return suma;
        }

        public void Zbroji()
        {
            Runda = Karte + Zvanja;
        }

        public void ZapisiRundu()
        {
            Zbroji();
            Bodovi.Add(Runda);
        }

        public string Str(int broj)
        {
            return broj.ToString();
        }

        public string IspisiRundu()
        {
            return Ime + " : " + Str(Runda) + "\n";
        }

        public void GotovaRunda()
        {
            Runda = 0;
            Runda = Karte + Zvanja;
            Karte = 0;
            Zvanja = 0;
        }

        public void Izbrisi()
        {
            Karte = 0;
            Zvanja = 0;
            Runda = 0;
            Bodovi = new List<int>();
        }

        public bool Pobjeda(int max)
        {
            if (Suma() >= max)
            {
                return true;
            }
            return false;
        }

        public int DoVan(int max)
        {
            return max - Suma();
        }

    }
}
