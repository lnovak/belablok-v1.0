﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BelaBlok
{
    public partial class Start : Form
    {
        public Engine engine = new Engine();
        public Start()
        {
            InitializeComponent();
        }

        private void Start_Load(object sender, EventArgs e)
        {
            //engine = new Engine()
        }

        private void bIzlaz_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bPostavke_Click(object sender, EventArgs e)
        {
            (new Postavke()).Show();
        }

        private void bPocetak_Click(object sender, EventArgs e)
        {
            (new Igra()).ShowDialog();
            this.Close();
        }

        private void bPovijest_Click(object sender, EventArgs e)
        {
            (new Povijest()).Show();
        }
    }
}
