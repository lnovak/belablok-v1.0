﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BelaBlok
{
    public partial class Postavke : Form
    {
        public Engine engine = new Engine();
        public Postavke()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Postavke_Load(object sender, EventArgs e)
        {
            tTim1.Text = engine.STim1;
            tTim2.Text = engine.STim2;
            tMax.Text = engine.MaxBodovi.ToString();
        }

        private void bSpremi_Click(object sender, EventArgs e)
        {
            if (tTim1.Text == tTim2.Text)
            {
                MessageBox.Show("Timovi nemogu imati ista imena");
            }
            else if (!tMax.Text.All(char.IsDigit))
            {
                MessageBox.Show("Max bodovi ne smiju sadržavati znakove");
            }
            else if (Int32.Parse(tMax.Text)<1 )
            {
                MessageBox.Show("Max bodovi ne smiju biti 0");
            }
            else
            {
                engine.PisiPostavke(tTim1.Text, tTim2.Text, Int32.Parse(tMax.Text));
                this.Close();
            }

        }

        private void bOdbaci_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tMax_TextChanged(object sender, EventArgs e)
        {
        }
    }
}
