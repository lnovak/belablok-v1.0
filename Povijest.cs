﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BelaBlok
{
    public partial class Povijest : Form
    {
        Engine engine = new Engine();
        public Povijest()
        {
            InitializeComponent();
        }

        private void bIzlaz_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bIzbrisi_Click(object sender, EventArgs e)
        {
            engine.IzbrisiPovijest();
            tPovijest.Text = engine.Povijest();
        }

        private void Povijest_Load(object sender, EventArgs e)
        {
            tPovijest.Text = engine.Povijest();
        }
    }
}
