﻿
namespace BelaBlok
{
    partial class Postavke
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lT1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tTim1 = new System.Windows.Forms.TextBox();
            this.tTim2 = new System.Windows.Forms.TextBox();
            this.tMax = new System.Windows.Forms.TextBox();
            this.bSpremi = new System.Windows.Forms.Button();
            this.bOdbaci = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lT1
            // 
            this.lT1.AutoSize = true;
            this.lT1.Location = new System.Drawing.Point(33, 37);
            this.lT1.Name = "lT1";
            this.lT1.Size = new System.Drawing.Size(39, 13);
            this.lT1.TabIndex = 0;
            this.lT1.Text = "Tim 1 :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tim 2 :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Igra do :";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // tTim1
            // 
            this.tTim1.Location = new System.Drawing.Point(88, 34);
            this.tTim1.Name = "tTim1";
            this.tTim1.Size = new System.Drawing.Size(100, 20);
            this.tTim1.TabIndex = 3;
            // 
            // tTim2
            // 
            this.tTim2.Location = new System.Drawing.Point(88, 67);
            this.tTim2.Name = "tTim2";
            this.tTim2.Size = new System.Drawing.Size(100, 20);
            this.tTim2.TabIndex = 4;
            // 
            // tMax
            // 
            this.tMax.Location = new System.Drawing.Point(88, 102);
            this.tMax.Name = "tMax";
            this.tMax.Size = new System.Drawing.Size(100, 20);
            this.tMax.TabIndex = 5;
            this.tMax.TextChanged += new System.EventHandler(this.tMax_TextChanged);
            // 
            // bSpremi
            // 
            this.bSpremi.Location = new System.Drawing.Point(36, 193);
            this.bSpremi.Name = "bSpremi";
            this.bSpremi.Size = new System.Drawing.Size(152, 23);
            this.bSpremi.TabIndex = 6;
            this.bSpremi.Text = "Spremi";
            this.bSpremi.UseVisualStyleBackColor = true;
            this.bSpremi.Click += new System.EventHandler(this.bSpremi_Click);
            // 
            // bOdbaci
            // 
            this.bOdbaci.Location = new System.Drawing.Point(36, 249);
            this.bOdbaci.Name = "bOdbaci";
            this.bOdbaci.Size = new System.Drawing.Size(152, 23);
            this.bOdbaci.TabIndex = 7;
            this.bOdbaci.Text = "Odbaci";
            this.bOdbaci.UseVisualStyleBackColor = true;
            this.bOdbaci.Click += new System.EventHandler(this.bOdbaci_Click);
            // 
            // Postavke
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(228, 321);
            this.Controls.Add(this.bOdbaci);
            this.Controls.Add(this.bSpremi);
            this.Controls.Add(this.tMax);
            this.Controls.Add(this.tTim2);
            this.Controls.Add(this.tTim1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lT1);
            this.Name = "Postavke";
            this.Text = "Bela Blok - Postavke";
            this.Load += new System.EventHandler(this.Postavke_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lT1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tTim1;
        private System.Windows.Forms.TextBox tTim2;
        private System.Windows.Forms.TextBox tMax;
        private System.Windows.Forms.Button bSpremi;
        private System.Windows.Forms.Button bOdbaci;
    }
}